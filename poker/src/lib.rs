use std::{
    collections::{BTreeMap, HashSet},
    str::FromStr,
};

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
enum Suit {
    Hearts,
    Spades,
    Diamonds,
    Clubs,
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
enum Rank {
    AceLow = 1,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Hash, Clone, Copy)]
struct Card {
    rank: Rank,
    suit: Suit,
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
enum HandRank {
    High { ranks: [Rank; 5] },
    Pair { rank: Rank, rest: [Rank; 3] },
    TwoPair { high: Rank, low: Rank, rest: [Rank; 1] },
    ThreeOfAKind { rank: Rank, rest: [Rank; 2] },
    Straight(Rank),
    Flush { ranks: [Rank; 5] },
    FullHouse { threes: Rank, twos: Rank },
    FourOfAKind { rank: Rank, rest: [Rank; 1] },
    StraightFlush(Rank),
}

#[derive(Debug, Ord, Eq, PartialEq, PartialOrd)]
struct Hand {
    cards: [Card; 5],
}

impl FromStr for Hand {
    type Err = InvalidCardFormat;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let cards: Vec<Card> = s.split_whitespace().map(|card_str| {
            let (r, s) = card_str.split_at(card_str.len() - 1);
            let rank = match r {
                // Rank::AceLow handled when calculating Straight(Flush)
                "2" => Rank::Two,
                "3" => Rank::Three,
                "4" => Rank::Four,
                "5" => Rank::Five,
                "6" => Rank::Six,
                "7" => Rank::Seven,
                "8" => Rank::Eight,
                "9" => Rank::Nine,
                "10" => Rank::Ten,
                "J" => Rank::Jack,
                "Q" => Rank::Queen,
                "K" => Rank::King,
                "A" => Rank::Ace,
                err => panic!("Unexpected rank {}", err)
            };
            let suit = match s {
                "H" => Suit::Hearts,
                "C" => Suit::Clubs,
                "D" => Suit::Diamonds,
                "S" => Suit::Spades,
                err => panic!("Unexpected suit {}", err)
            };
            Card { rank, suit }
        }).collect();

        Ok(Hand { cards: cards.try_into().unwrap() })
    }
}

#[derive(Debug)]
struct InvalidCardFormat;
impl Hand {
    fn rank(&self) -> HandRank {
        let mut rank_to_count = BTreeMap::new();
        for card in self.cards {
            rank_to_count
                .entry(card.rank)
                .and_modify(|count| *count += 1)
                .or_insert(1);
        }
        let mut count_to_rank: Vec<(usize, Rank)> = Vec::new();
        for (rank, count) in rank_to_count {
            count_to_rank.push((count, rank));
        }

        // sort by pool size, desc
        count_to_rank.sort_by(|a, b| a.0.cmp(&b.0));
        count_to_rank.reverse();

        match count_to_rank[..] {
            [(4, rank), (1, rest)] => HandRank::FourOfAKind { rank, rest: [rest] },
            [(3, threes), (2, twos)] => HandRank::FullHouse { threes, twos },
            [(3, rank), (1, k1), (1, k2)] => HandRank::ThreeOfAKind { rank, rest: [k1, k2] },
            [(2, high), (2, low), (1, rest)] => HandRank::TwoPair { high, low, rest: [rest] },
            [(2, rank), (1, k1), (1, k2), (1, k3)] => HandRank::Pair { rank, rest: [k1, k2, k3] },
            [(1, head), (1, t1), (1, t2), (1, t3), (1, t4)] => {
                let ranks = match [head, t1] {
                    [Rank::Ace, Rank::Five] => [t1, t2, t3, t4, Rank::AceLow],
                    _ => [head, t1, t2, t3, t4]
                };
                let is_straight = (ranks[0] as u8) - (ranks[4] as u8) == 4;
                let is_flush = HashSet::from(self.cards.map(|card| card.suit)).len() == 1;

                match (is_straight, is_flush) {
                    (true, true) => HandRank::StraightFlush(ranks[0]),
                    (true, false) => HandRank::Straight(ranks[0]),
                    (false, true) => HandRank::Flush { ranks },
                    (false, false) => HandRank::High { ranks },
                }
            }
            _ => panic!("Unrecognised pool size"),
        }
    }
}

/// Given a list of poker hands, return a list of those hands which win.
///
/// Note the type signature: this function should return _the same_ reference to
/// the winning hand(s) as were passed in, not reconstructed strings which happen to be equal.
pub fn winning_hands<'a>(hands: &[&'a str]) -> Vec<&'a str> {
    let mut hand_ranks = BTreeMap::new();
    for hand_str in hands {
        let hand: Hand = hand_str.parse().unwrap();
        hand_ranks
            .entry(hand.rank())
            .and_modify(|pool: &mut Vec<&str>| pool.push(hand_str))
            .or_insert(vec![hand_str]);
    }
    let (_, winning_hands) = hand_ranks.pop_last().expect("Unexpected result");
    winning_hands
}

